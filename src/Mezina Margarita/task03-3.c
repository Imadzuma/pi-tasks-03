#include <stdio.h>
#include <string.h>
int main()
{
	char s[256];
	fgets(s, 256, stdin);
	long i, a[256];
	for (i = 0; i < 256; ++i)
		a[i] = 0;
	for (i = 0; i < strlen(s) - 1; ++i)
		++a[s[i]];
	char c;
	for (i = 0; i < 256; ++i) 
		if (a[i] != 0) {
			c = i;
			printf("%c-%i,", c, a[i]);
		}
	printf("\b \n");
	return 0;
}